function Get-Locality {
    <#
    .SYNOPSIS
    This cmdlet attempts to parse out locations from the input

    .DESCRIPTION
    By default, the cmdlet is loaded with many countries, all 50 US states, and some cities. You can extend or overwrite each of these lists.

    The cmdlet will then attempt to guess each state, country, land (larger than single country), area (something within a state), region (something within a country), and city.

    It will create an object and leave any unresolved text available for review

    Countries sourced from: https://www.nationsonline.org/oneworld/countries_of_the_world.htm
    States sourced from: https://abbreviations.yourdictionary.com/articles/state-abbrev.html
    Capitols sourced from: https://www.50states.com/tools/thelist.htm
    Largest Cities sourced from: https://www.mymove.com/city-guides/compare/largest-us-cities/

    .PARAMETER InputObject
    The string to parse

    .PARAMETER Overwrite
    A hashtable containing one or more of the following keys: lands,countries,regions,states,areas,cities. Will replace any inbuilt lists with the values found in each key.

    .PARAMETER Append
    A hashtable containing one or more of the following keys: lands,countries,regions,states,areas,cities. Will append to any inbuilt lists with the values found in each key.

    .EXAMPLE
    PS> "NYC" | Get-Locality

    Land    :
    Country : United States
    Region  :
    State   : New York
    Area    :
    City    : New York City
    Unknown :

    .EXAMPLE
    PS> Get-Locality Reno

    Land    :
    Country :
    Region  :
    State   :
    Area    :
    City    :
    Unknown : Reno

    This example shows what happens when a location is unknown

    .EXAMPLE
    PS> Get-Locality -Append @{cities=@{name="Reno"}} Reno

    Land    :
    Country :
    Region  :
    State   :
    Area    :
    City    : Reno
    Unknown :

    This example shows how you can append a list of city hashtables to the cities list to recognize specific cities

    .EXAMPLE
    PS> Get-Locality -Append @{cities=@{name="Reno";state="Nevada"}} Reno

    Land    :
    Country : United States
    Region  :
    State   : Nevada
    Area    :
    City    : Reno
    Unknown :

    This example shows how tying a city to a state allows the state to get resolved to a country

        .EXAMPLE
    PS> Get-Locality "Carson City"

    Land    :
    Country : United States
    Region  :
    State   : Nevada
    Area    :
    City    : Carson City
    Unknown :

    This example showcases how any state capitol is a known city and can be resolved to larger geo units

    .EXAMPLE
    PS> Get-Locality "Carson City" -Overwrite @{cities=@{}}

    Land    :
    Country :
    Region  :
    State   :
    Area    :
    City    :
    Unknown : Carson City

    This example shows how overriding the cities list can make inbuilt known locations no longer known.

    .EXAMPLE
    PS> "Phoenix,Arizona","Switzerland","Ghana,Accra","Central America","Phoenix,Arizona,USA","England","Pacific NorthWest","pnw","Austin","Georgia" | Get-Locality |ft -auto

    Land            Country        Region             State   Area City    Unknown
    ----            -------        ------             -----   ---- ----    -------
                    United States                     Arizona      Phoenix
                    Switzerland
                    Ghana                                                  Accra
    Central America
                    United States                     Arizona      Phoenix
                    United Kingdom England
                    United States  Pacific North West
                    United States  Pacific North West
                    United States                     Texas        Austin
                    United States                     Georgia

    This example shows how abbreviations are all converted to the same base name and how any known locations resolve to their larger known geographic units as well.

    .NOTES
    ### Known Issue ###
    Georgia sucks. We will always assume that the state of georgia is meant, not the country:

    PS> Get-Locality "Georgia","Tbilisi,Georgia","Atlanta,Georgia"| ft

    Land Country       Region State   Area    City    Unknown
    ---- -------       ------ -----   ----    ----    -------
         United States        Georgia
         United States        Georgia Tbilisi
         United States        Georgia         Atlanta

    If you are using Georgia the country, you'll need to use the local name or the name Georgia_Country like so:

    PS> Get-Locality "Georgia","Tbilisi,Sakartvelo","Tbilisi,Georgia_Country","Atlanta,Georgia"| ft

    Land Country         Region State   Area City    Unknown
    ---- -------         ------ -----   ---- ----    -------
         United States          Georgia
         Georgia_Country                             Tbilisi
         Georgia_Country                             Tbilisi
         United States          Georgia      Atlanta

    #>
    [cmdletbinding()]
    param(
        [Parameter(ValueFromPipeline, Mandatory)]
        $InputObject,
        [hashtable]$Overwrite,
        [hashtable]$Append
    )

    begin {
        # Dynamically load any existing json files to populate these lists
        "lands", "countries", "regions", "states", "areas", "cities" | ForEach-Object {
            # If runtime doesn't want to use the existing file (-Overwrite), use their param instead
            if ($PSBoundparameters.Overwrite.$_) {
                [array]$obj = $PSBoundparameters.Overwrite.$_
            }
            elseif (Test-Path $PSScriptRoot\$_.json) {
                [array]$obj = Get-ChildItem $PSScriptRoot\$_.json | ForEach-Object { Get-Content $_.Fullname } | ConvertFrom-Json
            }
            else {
                # At least need an array object to add to
                [array]$obj = @()
            }

            # If runtime wants to add extra items to the list, they should get added here
            if ($PSBoundparameters.Append.$_) {
                $obj += $PSBoundparameters.Append.$_
            }

            # Need to ensure that we have only 1 entry in the list with the same value in the property "name"
            # Needs to be case insensitive detection and needs to not get sorted
            $out = [ordered]@{}
            $obj | Where-Object { $_.Name } | ForEach-Object {
                if (-not $out.Contains($_.Name)) {
                    $out.Add($_.Name, [PSCustomObject]$_)
                }
            }

            Set-Variable -Name $_ -Value $out.values

            # Get any external modules or commands that should get executed before or after processing
            $script:modules = @(Get-Module).Where{ $_.PrivateData -and $_.PrivateData.ContainsKey("Locality") }
            if (-not [string]::IsNullOrWhiteSpace($Locality_Pre)) {
                $script:pre = $Locality_Pre
            }
            else {
                $script:pre = ""
            }
            if (-not [string]::IsNullOrWhiteSpace($Locality_Post)) {
                $script:post = $Locality_Post
            }
            else {
                $script:post = ""
            }
        }
    }
    process {
        foreach ($item in $InputObject) {
            $item = Invoke-Hook -HookType Pre -InputObject $item
            $item = [Locality]::New($item, $Lands, $Countries, $Regions, $States, $Areas, $Cities)
            $item = Invoke-Hook -HookType Post -InputObject $item
            $item
        }
    }
}
