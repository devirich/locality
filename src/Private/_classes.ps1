class Locality {
    [string]$Land
    [string]$Country
    [string]$Region
    [string]$State
    [string]$Area
    [string]$City
    [string]$Unknown
    hidden [System.Collections.Generic.List[string]]$Leftover

    Locality([string]$Location, $Lands, $Countries, $Regions, $States, $Areas, $Cities) {
        # Need to split the base string into some basic components
        $this.Leftover = ($Location.Trim() -split "[,;:|]+ *").Trim()

        # Prioritize country over state, state over area, etc.
        $SearchOrder = @(
            @{name = "Country"; variable = "Countries" }
            @{name = "State"; variable = "States" }
            @{name = "Area"; variable = "Areas" }
            @{name = "Land"; variable = "Lands" }
            @{name = "Region"; variable = "Regions" }
            @{name = "City"; variable = "Cities" }
        )
        foreach ($item in $SearchOrder) {
            $names = "Land", "Country", "Region", "State", "Area", "City"
            $obj = Get-Variable $item.variable -ValueOnly
            $name = $item.name

            # Search all properties in all objects in the list except for the properties that get assigned on match ($names)
            $properties = $obj |
            ForEach-Object { $_.PSObject.Properties.Name } |
            Select-Object -Unique |
            Where-Object { $_ -notin $names }
            Write-Verbose "Searching properties: $properties"

            foreach ($property in $properties) {
                if ($this.Leftover -and -not $this.$name) {
                    Write-Verbose "Listing $name - $property"
                    # Create match list of the unmatched properties
                    if ($results = $obj | Where-Object { $_.$property -match "^$($this.Leftover -join '$|^')$" }) {
                        $this.$name = $results.name | Select-Object -First 1
                        "Found ${name} (for property- $property): {0}" -f $this.$name | Write-Verbose
                        $this.RemoveLeftover(($results.$property | Select-Object -First 1))
                        # If match found, check the $names properties on that object and set other data on the output object accordingly
                        foreach ($name in $names) {
                            if ($results.$name -and -not $this.$name) {
                                $this.$name = $results.$name | Select-Object -First 1
                                "Found context ($name): {0}" -f $this.$name | Write-Verbose
                                $this.RemoveLeftover($this.$name)
                            }
                        }
                    }
                    elseif ($name -eq "State") {
                        # States often get added to a line without any separater (e.g. `Boston MA`, `Tala Fla.`, or `Des Moines Iowa`)
                        # If the line ends with any state matches, peel that state part off the line
                        if ($results = $obj | Where-Object { $this.Leftover -match " $($_.$property -join '$| ')$" }) {
                            $this.$name = $results.name
                            "Found ${name}: {0}" -f $this.$name | Write-Verbose
                            $item, $rest = $this.Leftover.Where({ $_ -match " $($Results.$property)$" }, "Split", 1)
                            $this.Leftover = $rest
                            $this.Leftover.Add(($item -replace " $($Results.$property)"))
                            $this.Leftover = $this.Leftover | Where-Object { $_ }
                        }
                    }
                }
            }
            # When searching for state,
            # Washingtonions say state to remove ambiguity with Wash DC.
            # Their state should always end up as just the state name
            if ($name -eq "State" -and $this.Leftover -like "Washington State") {
                $results = $this.Leftover -like "Washington State" | Select-Object -First 1
                $this.$name = "Washington"
                "Found state match {0}" -f $this.$name | Write-Verbose
                $this.RemoveLeftover($results)
            }
            # When searching for area,
            # If any lefover lines end in ` county`, call that an area and remove.
            elseif ($name -eq "Area" -and -not $this.$name) {
                if ($results = $this.Leftover | Where-Object { $_ -match " County$" }) {
                    $this.$name = $results | Select-Object -First 1
                    "Found fuzzy match ($name): {0}" -f $this.$name | Write-Verbose
                    $this.RemoveLeftover($this.$name)
                }
            }
        }

        # When state is good and there are still leftovers, assign the leftover to the area.
        if ($this.State -and $this.Leftover) {
            $item = $this.Leftover | Select-Object -First 1
            $this.Leftover.Remove($item)
            if ([string]::IsNullOrWhiteSpace($this.Area)) {
                $this.Area = $item
                "Found (fuzzy) Area: {0}" -f $this.Area | Write-Verbose
            }
            else {
                $this.Unknown = $item
                "Found (fuzzy) Unkown: {0}" -f $this.Unknown | Write-Verbose
            }
        }

        # After the base searching is all done, do another pass with cities, states, and countries
        # Try to add the larger locations if the more specific location is known
        $this.ResolveLocations($Countries, $States, $Cities)

        # Leftover isn't a pretty object on output (its a list, not an array)
        # So Leftover has been set to hidden
        # Unknown which is an array is output instead
        if ($this.Leftover) {
            if ([string]::IsNullOrWhiteSpace($this.Unknown)) {
                $this.Unknown = $this.Leftover -join ", "
            }
            else {
                $this.Unknown += ", " + ($this.Leftover -join ", ")
            }
        }
    }

    RemoveLeftover([string]$item) {
        # Create a scriptblock with a here-string that contains a here-string to protect against injection accidents
        $sb = @"
param(`$i)
`$i -eq @"
$item
`"@
"@
        $sb = [scriptblock]::Create($sb)

        # Arrays.Remove() methods are all case sensitive. But FindIndex with a scriptblock predicate can choose NOT to be.
        # Use FindIndex with the sb above to do a case insensitive search for the input $item and then remove it from the array.
        if (($index = $this.Leftover.FindIndex($sb)) -ne -1) {
            Write-Verbose "found index $index"
            $this.Leftover.RemoveAt($index)
        }
        else {
            Write-Verbose "found no index for item $item"
        }
    }

    ResolveLocations($Countries, $States, $Cities) {

        if ($this.City -and -not $this.State) {
            # Cities that have a known state can be assigned to that state.
            if ($result = $Cities | Where-Object { $_.Name -eq $this.City -and $_.State }) {
                $this.State = $result.State
            }
        }
        if ($this.State -and -not $this.Country) {
            # States that have a known country can be assigned to that country.
            if ($result = $States | Where-Object { $_.Name -eq $this.State -and $_.Country }) {
                $this.Country = $result.Country
            }
        }
    }

    [string] ToString() {
        $output = ""
        @(
            "Unknown"
            "City"
            "Area"
            "State"
            "Region"
            "Country"
            "Land"
        ) | ForEach-Object {
            if (-not [string]::IsNullOrWhiteSpace($this.$_)) {
                $output += ", " + $this.$_
            }
        }
        return $output.Trim(', ')
    }
}
