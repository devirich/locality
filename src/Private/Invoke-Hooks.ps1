﻿function Invoke-Hook {
    # This function has two parts: Processing to do if there are module hooks and processing for a command hook
    [CmdletBinding()]
    param(
        [ValidateSet("Pre", "Post")]
        [Parameter(Mandatory)]
        [string]$HookType,

        $InputObject
    )

    # Part one: Module Hooks
    foreach ($mi in $script:modules) {
        try {
            # Only run if the hashtable pre/post properties exist and are set with actual text
            if (-not [string]::IsNullOrWhiteSpace($mi.PrivateData["Locality"][$HookType])) {
                Write-Verbose "Executing command $($mi.Name)\$($mi.PrivateData["Locality"][$HookType]) with data $InputObject"
                $InputObject = & "$($mi.Name)\$($mi.PrivateData["Locality"][$HookType])" $InputObject
            }
        }
        catch {
            Write-Warning "Failed to execute command '$($mi.Name)\$($mi.PrivateData["Locality"][$HookType])' with data $InputObject"
        }
    }

    # Part two: Command Hook
    # Need to use an if/else because script scope with dynamic text seems hard ($script:$HookType)
    if ($HookType -eq "Pre") {
        $command = $script:Pre
    }
    else {
        $command = $script:Post
    }
    # Don't need to validate -not [string]::IsNullOrWhiteSpace here because we already did that in the Begin block of the public function
    if ($command) {
        Write-Verbose "Executing command $command with data $InputObject"
        $InputObject = & $command -InputObject $InputObject
    }

    # Part three: Push data to the output stream
    $InputObject
}
