﻿Describe "Get-Locality" {
    Context "Regular parameters" {
        It "Detects lands" {

            $Actual = Get-Locality "Europe"
            $Expected = [PSCustomObject]@{
                Land    = "Europe"
                Country = $null
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "SouthernEurope"
            $Expected = [PSCustomObject]@{
                Land    = "Southern Europe"
                Country = $null
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Detects countries" {

            $Actual = Get-Locality "United Kingdom"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United Kingdom"
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "UK"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United Kingdom"
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Österreich"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "Austria"
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "US"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Detects regions" {
            $Actual = Get-Locality "Pacific North West"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = "Pacific North West"
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
            $Actual = Get-Locality "Pacific NorthWest"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = "Pacific North West"
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
            $Actual = Get-Locality "PacificNorthWest"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = "Pacific North West"
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
            $Actual = Get-Locality "pNw"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = "Pacific North West"
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "The South"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = "The South"
                State   = $null
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }

        It "Detects states" {

            $Actual = Get-Locality "Nevada"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "NV"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Nev."
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Washington State"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Washington"
                Area    = $null
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }

        It "Detects areas" {
            $Actual = Get-Locality "Tri-Cities" -Append @{areas = @{name = "Tri-Cities"; state = "Washington" } }
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Washington"
                Area    = "Tri-Cities"
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "NY County"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = $null
                Region  = $null
                State   = $null
                Area    = "NY County"
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Orange County,CA"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "California"
                Area    = "Orange County"
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Southern,TX"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Texas"
                Area    = "Southern"
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Orange County,Fla."
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Florida"
                Area    = "Orange County"
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Detects capitols" {
            $Actual = Get-Locality "Carson City"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "CarsonCity"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "CarsonCity,Nevada"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Carson City,Nevada"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected



            $Actual = Get-Locality "Carson City,NV"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Carson City NV"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Carson City Nev."
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Carson City Nevada"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Carson City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

        }
        It "Detects cities" {
            $Actual = Get-Locality "L.V."
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Las Vegas"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "L.V.,Nevada"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Nevada"
                Area    = $null
                City    = "Las Vegas"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Appends to db" {
            $Actual = Get-Locality "Colorado Springs"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = $null
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = "Colorado Springs"
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Colorado Springs" -Append @{cities = @{name = "Colorado Springs"; state = "Colorado" } }
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Colorado"
                Area    = $null
                City    = "Colorado Springs"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Overwrites db" {
            $Actual = Get-Locality "Denver"
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Colorado"
                Area    = $null
                City    = "Denver"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = Get-Locality "Denver" -Overwrite @{cities = @{name = "Denver" } }
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = $null
                Region  = $null
                State   = $null
                Area    = $null
                City    = "Denver"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Resolves locations" {
            $Actual = Get-Locality "The Hood" -Append @{areas = @{name = "The Hood"; city = "New York City" } }
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "New York"
                Area    = "The Hood"
                City    = "New York City"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
    }

    Context "Pipeline parameters" {
        It "Processes" {
            $Actual = "Southern,TX" | Get-Locality
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Texas"
                Area    = "Southern"
                City    = $null
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Appends correctly" {
            $Actual = "Colorado Springs" | Get-Locality
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = $null
                Region  = $null
                State   = $null
                Area    = $null
                City    = $null
                Unknown = "Colorado Springs"
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = "Colorado Springs" | Get-Locality -Append @{cities = @{name = "Colorado Springs"; state = "Colorado" } }
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Colorado"
                Area    = $null
                City    = "Colorado Springs"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
        It "Overwrites correctly" {
            $Actual = "Denver" | Get-Locality
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = "United States"
                Region  = $null
                State   = "Colorado"
                Area    = $null
                City    = "Denver"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Actual = "Denver" | Get-Locality -Overwrite @{cities = @{name = "Denver" } }
            $Expected = [PSCustomObject]@{
                Land    = $null
                Country = $null
                Region  = $null
                State   = $null
                Area    = $null
                City    = "Denver"
                Unknown = $null
            }
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
    }

    Context "Export" {
        It "Can export ToString all params" {
            $Actual = "Seattle" | Get-Locality
            $Actual.Land = "North America"
            $Actual.Region = "Pacific North West"
            $Actual.Area = "King County"
            $Actual.Unknown = "1234 Fake St"
            $Expected = "1234 Fake St, Seattle, King County, Washington, Pacific North West, United States, North America"
            Assert-Equivalent -Actual $Actual.ToString() -Expected $Expected

            $Expected = "1234 Fake st, Seattle, King County, Washington, Pacific North West, United States, North America"
            $Actual = "1234 Fake st, Seattle, King County, PNW, USA, North america" | Get-Locality
            Assert-Equivalent -Actual $Actual.ToString() -Expected $Expected

            $Expected = "1234 Fake st, Seattle, King County, Washington, Pacific North West, United States, North America"
            $Actual = $Expected | Get-Locality
            Assert-Equivalent -Actual $Actual.ToString() -Expected $Expected

            $Expected = "10223, 1234 Fake st, Seattle, King County, Washington, Pacific North West, United States, North America"
            $Actual = $Expected | Get-Locality | ForEach-Object ToString
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Expected = "1234 Fake st #204, Seattle, King County, Washington, Pacific North West, United States, North America"
            $Actual = $Expected | Get-Locality | ForEach-Object ToString
            Assert-Equivalent -Actual $Actual -Expected $Expected

            $Expected = "201, 202, 203, 1234 Fake st #204, 205, Seattle, King County, Washington, Pacific North West, United States, North America"
            $Actual = $Expected | Get-Locality | ForEach-Object ToString
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }

        It "Can export ToString properly" {
            $Actual = "Denver" | Get-Locality
            Assert-Equivalent -Actual $Actual.ToString() -Expected "Denver, Colorado, United States"

            $Actual = "Aardvarkkistan" | Get-Locality
            Assert-Equivalent -Actual $Actual.ToString() -Expected "Aardvarkkistan"

            $Actual = "Aardvarkkistan, Europe" | Get-Locality
            Assert-Equivalent -Actual $Actual.ToString() -Expected "Aardvarkkistan, Europe"

            $Actual = "PNW" | Get-Locality
            Assert-Equivalent -Actual $Actual.ToString() -Expected "Pacific North West, United States"
        }
    }
    Context "Check command hooks" {
        It "Can properly use pre- hook command" {
            $Expected = "Paris, Texas, United States"
            function global:Invoke-PreProcessing ($InputObject) { $InputObject -replace 'Paris', 'Paris,TX' }
            $global:Locality_pre = "Invoke-PreProcessing"
            $Actual = Get-Locality Paris | ForEach-Object ToString
            Assert-Equivalent -Actual $Actual -Expected $Expected
            $global:Locality_pre = ''
        }

        It "Can properly use post- hook command" {
            $Expected = "New York City, United States"
            function global:Invoke-PostProcessing ($InputObject) { $InputObject.State = ''; $InputObject }
            $global:Locality_post = "Invoke-PostProcessing"
            $Actual = Get-Locality NYC | ForEach-Object ToString
            $global:Locality_post = ''
            Assert-Equivalent -Actual $Actual -Expected $Expected
        }
    }
}
