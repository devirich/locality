# Locality

Locality is a PowerShell module for parsing location strings into more useful geographic units.

## Description

By default, the Get-Locality cmdlet is loaded with many countries, all 50 US states, and some cities. You can extend or overwrite each of these lists.

The cmdlet will then attempt to guess each state, country, land (larger than single country), area (something within a state), region (something within a country), and city.

It will create an object and leave any unresolved text available for review

## Installation

If you have the [PowerShellGet](https://msdn.microsoft.com/powershell/gallery/readme) module installed you can enter the following command:

    Install-Module Locality

## Examples
    PS> "NYC" | Get-Locality

    Land    :
    Country : United States
    Region  :
    State   : New York
    Area    :
    City    : New York City
    Unknown :

This example shows what happens when a location is unknown

    PS> Get-Locality Reno

    Land    :
    Country :
    Region  :
    State   :
    Area    :
    City    :
    Unknown : Reno


This example shows how you can append a list of city hashtables to the cities list to recognize specific cities

    PS> Get-Locality -Append @{cities=@{name="Reno"}} Reno

    Land    :
    Country :
    Region  :
    State   :
    Area    :
    City    : Reno
    Unknown :


This example shows how tying a city to a state allows the state to get resolved to a country

    PS> Get-Locality -Append @{cities=@{name="Reno";state="Nevada"}} Reno

    Land    :
    Country : United States
    Region  :
    State   : Nevada
    Area    :
    City    : Reno
    Unknown :


This example showcases how any state capitol is a known city and can be resolved to larger geo units

    PS> Get-Locality "Carson City"

    Land    :
    Country : United States
    Region  :
    State   : Nevada
    Area    :
    City    : Carson City
    Unknown :

This example shows how overriding the cities list can make inbuilt known locations no longer known.

    PS> Get-Locality "Carson City" -Overwrite @{cities=@{}}

    Land    :
    Country :
    Region  :
    State   :
    Area    :
    City    :
    Unknown : Carson City

This example shows how abbreviations are all converted to the same base name and how any known locations resolve to their larger known geographic units as well.

    PS> "Phoenix,Arizona","Switzerland","Ghana,Accra","Central America","Phoenix,Arizona,USA","England","Pacific NorthWest","pnw","Austin","Georgia" | Get-Locality |ft -auto

    Land            Country        Region             State   Area City    Unknown
    ----            -------        ------             -----   ---- ----    -------
                    United States                     Arizona      Phoenix
                    Switzerland
                    Ghana                                                  Accra
    Central America
                    United States                     Arizona      Phoenix
                    United Kingdom England
                    United States  Pacific North West
                    United States  Pacific North West
                    United States                     Texas        Austin
                    United States                     Georgia


## Extensibility

You can add more items (or replace) to each of the 6 lists used through the `-Append` and `-Overwrite` hashtable parameters:

    @{
        cities    = @(
            @{name = "Boston" }
            @{name = "SD"; State = "California" }
        )
        countries = @(
            @{name = "Your Country"; alt = "YourCountry" }
        )
    }

### Command Hook

There is also a hook in this module that allows you to define a command to be run on each item before and after it is run.
You register what command to run by populating the variables `$Locality_Pre` and `$Locality_Post` with the name of the command you would like ran.

This command hook extensibility has little practical application because you already have the input object before processing and you have the plain object after.
This functionality has been added for me to practice the pattern of adding a hook in for a module. Maybe it will be useful for someone to look at the commit one day.

As an example of registering the post hook, you could define a function and register it like so in order to make it so that EVERY object that gets processed will have its state field stripped:

~~~ps
function Invoke-Post ($InputObject) {$InputObject.State = "";$InputObject}
$Locality_post = "Invoke-Post"
~~~

Because the pre hook works on only the input string, you could do virtually the same commands for creating a pre- hook to run before any data gets processed. This could be to fixup or eliminate improper spellings or add a common city/state pairing or more.

In the default set, Paris is unknown. But with this pre- hook setup, anytime a locality to parse says "Paris", it would get clarified to be "Paris, Texas"
~~~ps
Get-Locality Paris
function Invoke-Pre ($InputObject) {$InputObject -replace "Paris","Paris,TX"}
$Locality_post = "Invoke-Pre"
Get-Locality Paris
~~~

### Module Hooks

You can also create modules to be discoverable by Locality where they will have a custom command executed prior to and/or after processing. Like a command hook with this module, this module hook extensibility is primarily for my own practice.

Using a module hook is done by placing a Locality hashtable in your module manifest's `PrivateData` section. You populate the hashtable keys `Pre` and `Post` with values that match up to what command you want run from your module before and after processing:

~~~ps
# ...
PrivateData = @{
    Locality = @{
        Pre = "Get-ExtraProcessingDone"
        Post = "Set-EnvelopeStickiness"
    }
    PSData = @{
# ...
~~~

You can leave either blank or not defined without issue.
## Acknowledgements

Countries sourced from: https://www.nationsonline.org/oneworld/countries_of_the_world.htm

States sourced from: https://abbreviations.yourdictionary.com/articles/state-abbrev.html

Capitols sourced from: https://www.50states.com/tools/thelist.htm

Largest Cities sourced from: https://www.mymove.com/city-guides/compare/largest-us-cities/
